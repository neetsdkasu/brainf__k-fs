//----------------------------------------
//
// Brainfuck Interpreter Core
//       port from my haskell implement
//
// author : Leonardone @ NEETSDKASU
// license: MIT License
//----------------------------------------
module Brainfuck

open neetsdkasu.utils.Collections
open neetsdkasu.utils.Core.OpEx

type Code = Command list
and  Command =
    | AddIndexCommand of int
    | AddValueCommand of int
    | WhileCommand    of Code
    | InputCommand
    | OutputCommand

type TokenType =
    | AddIndex
    | SubIndex
    | AddValue
    | SubValue
    | While
    | Wend
    | Input
    | Output
    | Comment

type InputHandle  = System.IO.TextReader
type OutputHandle = System.IO.TextWriter

type Index    = int
type Memory   = Map<int, int>
type JumpList = Code list
type TokenRef = Map<TokenType, seq<string>>

type Builder  = Builder  of Map<string, TokenType>

type Compiler = Compiler of (string * TokenType) list

type Machine  = Machine  of Index * Memory


let newMachine : Machine = Machine(0, Map.empty)


let run (inputH : InputHandle) (outputH : OutputHandle) (Machine(index, mem)) (code : Code) : Machine =
    let rec excute (index : Index) (mem : Memory) (jump : JumpList) (code : Code) : Machine =
        match (jump, code) with
        | ([], [])                    -> Machine(index, mem)
        | (ret::jump, [])             -> excute index mem jump ret
        | (_, AddIndexCommand(p)::cs) -> excute (index + p) mem jump cs
        | (_, AddValueCommand(p)::cs) -> excute index (MapEx.insertWith (+) index p mem) jump cs
        | (_, WhileCommand(loop)::cs) -> match Map.tryFind index mem with
                                         | Some(v) when v <> 0 -> excute index mem (code::jump) loop
                                         | _                   -> excute index mem jump         cs
        | (_, InputCommand      ::cs) -> excute index (Map.add index (inputH.Read()) mem) jump cs
        | (_, OutputCommand     ::cs) -> let ch = MapEx.findWithDefault 0 index mem
                                         outputH.Write(char ch)
                                         excute index mem jump cs
    excute index mem [] code


let once (inputH : InputHandle) (outputH : OutputHandle) (code : Code) : unit =
    run inputH outputH newMachine code
    |> ignore


let newBuilder : Builder = Builder(Map.empty)


let set (tokentype : TokenType) (tokens : string list) (Builder(alltokens)) : Builder =
    Builder(tokens
            |> List.filter ((<>) "")
            |> List.fold (fun m x -> Map.add x tokentype m) alltokens
            )


let buildCompiler (Builder(alltokens)) : Compiler =
    Compiler(alltokens
             |> Map.toList
             |> List.sortBy ((~-) << String.length << fst)
             )

let withComment (toks : string list) (Compiler(alltokens)) : Compiler =
    Compiler((toks
              |> List.filter ((<>) "")
              |> List.map (fun t -> (t, Comment))
              |> List.append
              ) alltokens
             )


let standardCompiler : Compiler =
    newBuilder
    |> set AddIndex [">"]
    |> set SubIndex ["<"]
    |> set AddValue ["+"]
    |> set SubValue ["-"]
    |> set While    ["["]
    |> set Wend     ["]"]
    |> set Input    [","]
    |> set Output   ["."]
    |> buildCompiler


let standardCompilerWithComment : Compiler =
    withComment ["#"] standardCompiler


let getTokenList (Compiler(alltokens)) (typ : TokenType) : string list =
    alltokens
    |> List.filter ((=) typ << snd)
    |> List.map fst


let compile (Compiler(alltokens)) (source : string) : Code =
    let check (ix : int) ((tok, _) : string * TokenType) : bool =
        System.String.CompareOrdinal(source, ix, tok, 0, String.length tok) = 0
    let skip (ix : int) : int =
        let nx = source.IndexOf('\n', ix)
        if nx < 0 then String.length source else nx
    let rec tokenize (ts : TokenType list) (ix : int) : TokenType list =
        if ix = String.length source then
            ts
        else
            match List.tryFind (check ix) alltokens with
               | Some(_, Comment) -> tokenize ts (skip ix)
               | Some(tok, typ)   -> tokenize (typ::ts) (ix + String.length tok)
               | None             -> tokenize ts (ix + 1)
    let rec parse (code : Code) (stack : Code list) (toks : TokenType list) : Code =
        match (toks, code, stack) with
        | ([], _, []) -> code
        | ([], _,  _) -> failwith "not found While-Wend pair"
        | (AddIndex::ts, AddIndexCommand(-1)::cs, _) -> parse cs stack ts
        | (AddIndex::ts, AddIndexCommand( p)::cs, _) -> parse (AddIndexCommand(p + 1)::cs) stack ts
        | (SubIndex::ts, AddIndexCommand( 1)::cs, _) -> parse cs stack ts
        | (SubIndex::ts, AddIndexCommand( p)::cs, _) -> parse (AddIndexCommand(p - 1)::cs) stack ts
        | (AddValue::ts, AddValueCommand(-1)::cs, _) -> parse cs stack ts
        | (AddValue::ts, AddValueCommand( p)::cs, _) -> parse (AddValueCommand(p + 1)::cs) stack ts
        | (SubValue::ts, AddValueCommand( 1)::cs, _) -> parse cs stack ts
        | (SubValue::ts, AddValueCommand( p)::cs, _) -> parse (AddValueCommand(p - 1)::cs) stack ts
        | (AddIndex::ts, _,  _) -> parse (AddIndexCommand( 1)::code) stack ts
        | (SubIndex::ts, _,  _) -> parse (AddIndexCommand(-1)::code) stack ts
        | (AddValue::ts, _,  _) -> parse (AddValueCommand( 1)::code) stack ts
        | (SubValue::ts, _,  _) -> parse (AddValueCommand(-1)::code) stack ts
        | (Input   ::ts, _,  _) -> parse (InputCommand       ::code) stack ts
        | (Output  ::ts, _,  _) -> parse (OutputCommand      ::code) stack ts
        | (Wend    ::ts, _,  _) -> parse [] (code::stack) ts
        | (While   ::ts, _, []) -> failwith "not found While-Wend pair"
        | (While   ::ts, _, rest::stack) -> parse (WhileCommand(code)::rest) stack ts
        | _                     -> failwith "never reach"
    tokenize [] 0
    |> parse [] []


let decompile (compiler : Compiler) (code : Code) : string =
    let tokens : TokenRef =
        [AddIndex; SubIndex; AddValue; SubValue; While; Wend; Input; Output]
        |> List.map (fun typ -> typ, SeqEx.cycle <| match getTokenList compiler typ with
                                                     | []   -> [""]
                                                     | toks -> toks )
        |> Map.ofList
    let getTokens (tokens : TokenRef) (typ : TokenType) : string =
        Seq.head <| Map.find typ tokens
    let rotTokens (tokens : TokenRef) (typs : TokenType list) : TokenRef =
        List.fold (flip <| MapEx.adjust SeqEx.tail) tokens typs
    let rec convert (tokens : TokenRef) (code : Code) : string =
        match code with
        | []          -> ""
        | command::cs ->
            let get = getTokens tokens
            let rot = rotTokens tokens
            let (tokens2, fragment) =
                match command with
                | AddIndexCommand(p) -> let t = if p < 0 then SubIndex else AddIndex
                                        rot [t], String.replicate (abs p) (get t)
                | AddValueCommand(p) -> let t = if p < 0 then SubValue else AddValue
                                        rot [t], String.replicate (abs p) (get t)
                | WhileCommand(loop) -> rot [While; Wend], get While + convert tokens loop + get Wend
                | InputCommand       -> rot [Input], get Input
                | OutputCommand      -> rot [Output], get Output
            fragment + convert tokens2 cs
    convert tokens code