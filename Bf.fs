//----------------------------------------
//
// Brainfuck Interpreter
//       port from my haskell implement
//
// author : Leonardone @ NEETSDKASU
// license: MIT License
//----------------------------------------
module Main


open Brainfuck


let readFile (file : string) : string =
    System.IO.File.ReadAllText(file)


let usage (compiler : Compiler) : unit = do
    let get = String.concat " " << getTokenList compiler
    let prog = System.AppDomain.CurrentDomain.FriendlyName
    printfn "Brainfuck Interpreter"
    printfn "  usage: %s <source file>" prog
    printfn ""
    printfn "  commands:"
    printfn "    increment pointer: %s" <| get AddIndex
    printfn "    decrement pointer: %s" <| get SubIndex
    printfn "    increment value  : %s" <| get AddValue
    printfn "    decrement value  : %s" <| get SubValue
    printfn "    begin of loop    : %s" <| get While
    printfn "    end of loop      : %s" <| get Wend
    printfn "    input from stdin : %s" <| get Input
    printfn "    output to stdout : %s" <| get Output
    printfn "    line comment     : %s" <| get Comment
    

let defaultCompiler : Compiler =
    newBuilder
    |> set AddIndex [">"; "Ri"] // increment pointer
    |> set SubIndex ["<"; "Le"] // decrement pointer
    |> set AddValue ["+"; "In"] // increment value
    |> set SubValue ["-"; "De"] // decrement value
    |> set While    ["["; "Be"] // begin of loop
    |> set Wend     ["]"; "En"] // end of loop
    |> set Input    [","; "Ch"] // input from stdin
    |> set Output   ["."; "Sh"] // output to stdout
    |> buildCompiler
    |> withComment  ["#"; "//"] // line comment
    

let parseArgs (args : string list) : bool * int * string * string =
    let rec parse (a : string list) (res : bool * int * string * string) : bool * int * string * string =
        let (u, t, f, c) = res
        match a with
        | []             -> res
        | ""        ::xs -> parse xs res
        | "-d"      ::xs -> parse xs (u, 0, f, c)        // use defaultCompiler
        | "-s"      ::xs -> parse xs (u, 1, f, c)        // use standardCompiler
        | "-sw"     ::xs -> parse xs (u, 2, f, c)        // use standardCompilerWithComment
        | "-c"::cmds::xs -> parse xs (u, 3, f, cmds)     // use customCompiler
        | "-u"      ::xs -> parse xs (true, t, f, c)     // show usage
        | "-f"::file::xs -> parse xs (false, t, file, c) // source file
        | file      ::xs -> parse xs (false, t, file, c) // source file
    parse args (true, 0, "", "")


let customCompiler (text : string) : Compiler =
    let cmds : string list [] =
        text + String.replicate 9 "\n"
        |> fun s -> s.Split('\n')
        |> Array.map (fun s -> s.Trim().Split(' ') |> Array.toList)
    newBuilder
    |> set AddIndex cmds.[0]  // increment pointer
    |> set SubIndex cmds.[1]  // decrement pointer
    |> set AddValue cmds.[2]  // increment value
    |> set SubValue cmds.[3]  // decrement value
    |> set While    cmds.[4]  // begin of loop
    |> set Wend     cmds.[5]  // end of loop
    |> set Input    cmds.[6]  // input from stdin
    |> set Output   cmds.[7]  // output to stdout
    |> buildCompiler
    |> withComment  cmds.[8]  // line comment
    
    
[<EntryPoint>]
let main args =
    let (u, t, f, c) = parseArgs <| Array.toList args
    let compiler =
        match t with
        | 0 -> defaultCompiler
        | 1 -> standardCompiler
        | 2 -> standardCompilerWithComment
        | 3 -> customCompiler <| readFile c
        | _ -> failwith "never reached here"
    if u then
        usage compiler
    else
        readFile f
        |> compile compiler
        |> once stdin stdout 
    0