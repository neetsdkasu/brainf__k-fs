//------------------------------------
// Operator Extentions
//
// version: 1.0
// author : Leonardone @ NEETSDKASU
// license: MIT License
//------------------------------------
namespace neetsdkasu.utils.Core


module OpEx =
    
    //
    // flip first and second arguments of function
    //
    // val flip : ('a -> 'b -> 'c) -> 'b -> 'a -> 'c
    //
    let flip fn a b = fn b a
      
    
    //
    // swap tuple values
    //
    // val swap : 'a * 'b -> 'b * 'a
    //
    let swap (a, b) = b, a
    
    
    //
    // make tuple
    //
    // val tuple : 'a -> 'b -> 'a * 'b
    //
    let tuple a b = a, b
    
    
    //
    // make tuple with 3 values
    //
    // val tuple3 : 'a -> 'b -> 'c -> 'a * 'b * 'c
    //
    let tuple3 a b c = a, b, c
    
    
    //
    // make tuple with 4 values
    //
    // val tuple4 : 'a -> 'b -> 'c -> 'd -> 'a * 'b * 'c * 'd
    //
    let tuple4 a b c d = a, b, c, d
    
    
    //
    // make tuple with 5 values
    //
    // val tuple5 : 'a -> 'b -> 'c -> 'd -> 'e -> 'a * 'b * 'c * 'd * 'e
    //
    let tuple5 a b c d e = a, b, c, d, e
    
    
    